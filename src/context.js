const ThemeContext = React.createContext('light')

<ThemeContext.Provider value={this.state.theme}>
{this.props.children}
</ThemeContext.Provider>

<ThemeContext.Consumer>
{val => <div>{val}</div>}
</ThemeContext.Consumer>