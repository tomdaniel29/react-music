import axios from 'axios';

const searchUrl = 'https://www.thecocktaildb.com/api/json/v1/1/filter.php';
const lookupUrl = 'https://www.thecocktaildb.com/api/json/v1/1/lookup.php';

export function getCocktails(search = '') {
	let params = {
		i: search
	};

	if(!search) {
		params.a = 'Alcoholic';
	}

	return axios.get(searchUrl, {params})
				.then(response => {
					console.log('request: ', response);
					let drinks = response.data.drinks || [];
					return drinks.slice(0, 50);
				});
}

export function getCocktail(id) {
	let params = {
		i: id
	}

	return axios.get(lookupUrl, {params})
				.then(response => {
					console.log('request cocktail: ', response);
					let drinks = response.data.drinks || [];
					return drinks[0];
				})
}