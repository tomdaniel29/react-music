import React from 'react';
import {Switch, Route} from 'react-router-dom';

import Home from '../home/home';
import Single from '../single/single';

export default (props) => (
	<div className="main">
		<Switch>
			<Route exact path="/" component={({history}) => (
				<Home history={history} cocktails={props.cocktails} />
			)} />
			<Route path="/cocktail/:number" component={Single} />
		</Switch>
	</div>
)
