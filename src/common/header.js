import React, {Component} from 'react';

export default class extends Component {
	constructor(props) {
		super(props);
		this.searchInput = React.createRef();
		this.handleSearchClick = this.handleSearchClick.bind(this);
	}

	handleSearchClick() {
		this.props.handleSearchChange(this.searchInput.current.value);
	}

	render() {
		return (
			<div className="header">
			<div className="header-title"><span>Cocktail Party</span></div>
			<div className="search-wrapper">
				<input type="text" className="search-input" ref={this.searchInput} />
				<button className="search-button" onClick={this.handleSearchClick}>Search</button>
			</div>
		</div>
		)
	}
}