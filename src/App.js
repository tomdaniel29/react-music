import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import {getCocktails, getCocktail} from './services/cocktails';
import Header from './common/header';
import Home from './home/home';
import Single from './single/single';

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			cocktails: [],
			search: ''
		}

		this.loadCocktail = this.loadCocktail.bind(this);
		this.handleSearchChange = this.handleSearchChange.bind(this);
	}

	componentDidMount() {
		this.loadCocktails();
	}

	loadCocktails(search = '') {
		getCocktails(search).then(cocktails => {
			this.setState({cocktails});
		});
	}

	loadCocktail(id) {
		return getCocktail(id).then(cocktail => {
			this.setState({cocktail});
		});
	}

	handleSearchChange(search) {
		console.log(search);
		this.loadCocktails(search);
		//this.setState(search);
	}

	render() {
		return (
			<React.Fragment>
				<Header handleSearchChange={this.handleSearchChange} />
				<div className="main">
					<Switch>
						<Route exact path="/" render={({history}) => (
							<Home history={history} cocktails={this.state.cocktails} />
						)} />
						<Route path="/cocktail/:number" render={({match}) => (
							<Single loadCocktail={this.loadCocktail} cocktail={this.state.cocktail} id={match.params.number} />
						)} />
					</Switch>
				</div>
			</React.Fragment>
		)
	}
}

export default App;
