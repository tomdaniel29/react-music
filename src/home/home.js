import React, {Component} from 'react';
import HomeCocktail from './home-cocktail';

export default class extends Component {
	constructor(props) {
		super(props);
		this.handleClickOnHomeCocktail = this.handleClickOnHomeCocktail.bind(this);
	}

	handleClickOnHomeCocktail(id) {
		this.props.history.push(`/cocktail/${id}`);
	}

	render() {
		return (
			<div className="home-cocktails">
				{this.props.cocktails.map(cocktail => (
					<HomeCocktail key={cocktail.idDrink} cocktail={cocktail} handleClickOnHomeCocktail={this.handleClickOnHomeCocktail} />
				))}
			</div>
		)
	}
}