import React from 'react';
import {Link} from 'react-router';

export default (props) => {
	let {idDrink, strDrink, strDrinkThumb} = props.cocktail;
	return (
		<div className="home-cocktail" onClick={() => props.handleClickOnHomeCocktail(idDrink)}>
			{/* <Link to="/cocktail/id">test</Link> */}
			<img src={strDrinkThumb} alt={strDrink}/>
			<div className="home-cocktail-title">
				<span>{strDrink}</span>
			</div>
		</div>
	)
}