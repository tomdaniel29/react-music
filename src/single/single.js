import React, {Component} from 'react';

export default class extends Component {
	componentDidMount() {
		this.props.loadCocktail(this.props.id);
	}

	render() {
		return (
			<div className="single-cocktail">
				{
					this.props.cocktail ? (
						<pre>
							{JSON.stringify(this.props.cocktail, null, 2)}
						</pre>
					) : (
						<div>Loading</div>
					)
				}
			</div>
		)
	}
}